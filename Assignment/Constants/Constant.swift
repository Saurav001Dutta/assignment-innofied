//  Constant.swift
//  Assignment
//
//  Created by appledev2 on 06/08/20.
//  Copyright © 2020 Saurav Dutta. All rights reserved.
//

import Foundation

// MARK: Pagination
var PER_PAGE_COUNT = 8


// MARK: Networking
var URL_WITH_PROTOCOL = "https://reqres.in/api/users"
//Method:GET Query: page={page_number},​per_page={page_size}

// MARK: Url response status code
var STATUS_CODE_REQUEST_TIMEOUT: Int = 1001
var STATUS_CODE_SUCCESS : Int = 200
var STATUS_CODE_SUCCESS_CREATED : Int = 201
var STATUS_CODE_UNAUTHORIZED_ACCESS : Int = 400
var STATUS_CODE_UNABLE_TO_CONNECT : Int = 404
var STATUS_CODE_INTERNAL_SERVER_ERROR : Int = 500
var STATUS_CODE_NIL_DATA : Int = 1501
var STATUS_CODE_UNKNOWN_ERROR : Int = 1502

