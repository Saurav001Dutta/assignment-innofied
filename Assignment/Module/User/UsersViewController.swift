//
//  UsersViewController.swift
//  Assignment
//
//  Created by appledev2 on 06/08/20.
//  Copyright © 2020 Saurav Dutta. All rights reserved.
//https://reqres.in/api/users?page=1&per_page=10

import UIKit

class UsersViewController: UIViewController {

    @IBOutlet var tblViewUser: UITableView!
    var pageNo :Int!
    var total = 0
    var total_pages = 1
    
    var arrUsers = [Users]()
    var isflag = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pageNo = 1
        self.registerCells()
        self.fetchData()
        self.pullToRefresh()
    }
    
    // MARK: - Network Call
    func generateQueryString() -> URL{
        return URL(string:URL_WITH_PROTOCOL + "?page=\(self.pageNo ?? 1)&per_page=\(PER_PAGE_COUNT)")!
    }
    
    func fetchData(){
        self.reloadTableView()
        SDNetworkManager.sharedInstance.callService(url: self.generateQueryString(), httpMethod: "GET", completion: {(response) in
            let userData = (response["Data"] as! [String:Any])["data"] as! Array<[String:Any]>
            
            let prevTotal = self.total
            self.total = (response["Data"] as! [String:Any])["total"] as? Int ?? 0
            self.total_pages = (response["Data"] as! [String:Any])["total_pages"] as? Int ?? 0
            
            //To avoid repeating items
            if prevTotal != 0{
                
                if (self.total == prevTotal){
                    if (self.arrUsers.count < self.total){
                         self.arrUsers += self.updateUserModel(userData)
                        self.isflag = true
                    }
                }else{
                    self.arrUsers.removeLast(prevTotal%PER_PAGE_COUNT)
                    self.arrUsers += self.updateUserModel(userData)
                    self.isflag = true
                }
                
            }else{
                
              self.arrUsers += self.updateUserModel(userData)
              self.isflag = true
                
            }
            
            self.reloadTableView()
            DispatchQueue.main.async {
                SDRefreshController.sharedInstance.endRefreshing()
            }
 
        })
    }
    
    func reloadTableView(){
        DispatchQueue.main.async {
            self.tblViewUser.reloadData()
        }
    }
    
    // MARK: - Updating User Model
    func updateUserModel(_ arr: Array<[String:Any]>!) -> [Users]{
        
        var tempUserArray = [Users]()
        for singleUser in arr {
           
            let _id = singleUser["id"] as? Int ?? 0
            let _email = singleUser["email"] as? String ?? ""
            let _first_name = singleUser["first_name"] as? String ?? ""
            let _last_name = singleUser["last_name"] as? String ?? ""
            let _avatar = singleUser["avatar"] as? String ?? ""

            let user = Users(id: _id, email: _email, first_name: _first_name, last_name: _last_name,avatar: _avatar)
            tempUserArray.append(user)
        }
        return tempUserArray
    }
    
    // MARK: - Registration(Reusuable cell)..
    func registerCells(){
        self.tblViewUser.register(UINib(nibName: "UserListTableViewCell", bundle: nil), forCellReuseIdentifier: "UserListTableViewCell")
        self.tblViewUser.register(UINib(nibName: "LoadingTableViewCell", bundle: nil), forCellReuseIdentifier: "LoadingTableViewCell")
    }
    
    // MARK: - Refresh Control & Refresh Action
    func pullToRefresh(){
        SDRefreshController.sharedInstance.addTarget(self, action: #selector(refresh(_:)), for: UIControl.Event.valueChanged)
        self.tblViewUser.addSubview(SDRefreshController.sharedInstance)
    }
    
    @objc func refresh(_ sender: Any) {
        self.arrUsers.removeAll()
        pageNo = 1
        self.fetchData()
    }
    
}

// MARK: - TableView Delegate & Datasource
extension UsersViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !isflag{
           return self.arrUsers.count + 1
        }
        return self.arrUsers.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if  !isflag && indexPath.row == self.arrUsers.count{
            let cell = tableView.dequeueReusableCell(withIdentifier: "LoadingTableViewCell", for: indexPath) as! LoadingTableViewCell
            cell.activityIndicator.startAnimating()
            return cell
            
        }else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "UserListTableViewCell", for: indexPath) as! UserListTableViewCell
        
            let singleUser = self.arrUsers[indexPath.row]
            cell.lblName.text = singleUser.first_name + singleUser.last_name
            cell.lblEmail.text = singleUser.email
        
            let imgURL = singleUser.avatar
            if imgURL != ""{
                cell.imgViewProfile.load(url:URL(string: imgURL)!, onCompletion: {
                    cell.activityIndicator.isHidden = true
                })
            }else{
                    cell.activityIndicator.isHidden = true
            }
        
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        var needNetworkingCall = false
        if (self.total_pages != 0 && self.total != 0 && indexPath.row > self.arrUsers.count-2 && isflag){
            if (self.total_pages > pageNo ){
                pageNo += 1
                needNetworkingCall = true
            }
            else if (self.total_pages == pageNo && (self.arrUsers.count%PER_PAGE_COUNT != 0)){
                needNetworkingCall = true
            }
        }

        if needNetworkingCall{
            DispatchQueue.global().asyncAfter(deadline: .now() + 1.0) { ///This line is added for no reson as API is fetching data very quickly so loader is not showing
                self.fetchData()
            }
        }
    }
}
