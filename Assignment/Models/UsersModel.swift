//
//  UsersModel.swift
//  Assignment
//
//  Created by appledev2 on 06/08/20.
//  Copyright © 2020 Saurav Dutta. All rights reserved.
//

import Foundation

class Users:Codable{
    
    var id : Int = 0
    var email : String = ""
    var first_name: String = ""
    var last_name: String = ""
    var avatar: String = ""
    
    required init(id: Int, email: String, first_name: String, last_name: String,avatar: String) {
        
        self.id = id
        self.email = email
        self.first_name = first_name
        self.last_name = last_name
        self.avatar = avatar
    }
}
