//
//  AssignmentExtension.swift
//  Assignment
//
//  Created by appledev2 on 06/08/20.
//  Copyright © 2020 Saurav Dutta. All rights reserved.
//

import Foundation
import UIKit


// MARK: - ImageView Extension
extension UIImageView {
    func load(url: URL, onCompletion:@escaping () -> Void) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                        onCompletion()
                    }
                }
            }
        }
    }
}
