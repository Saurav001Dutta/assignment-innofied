//
//  RefreshController.swift
//  Assignment
//
//  Created by appledev2 on 06/08/20.
//  Copyright © 2020 Saurav Dutta. All rights reserved.
//

import Foundation
import UIKit

class SDRefreshController: UIRefreshControl {
    
    static let sharedInstance = SDRefreshController()
    
    override init() {
        super.init()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
