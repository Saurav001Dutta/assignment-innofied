//
//  NetworkManager.swift
//  Assignment
//
//  Created by appledev2 on 06/08/20.
//  Copyright © 2020 Saurav Dutta. All rights reserved.
//

import Foundation

class SDNetworkManager{
    
    // MARK: Singleton
    static let sharedInstance = SDNetworkManager()
    private init() {}
    
    
    // MARK: Webservice Method using URLSession
    func callService( url : URL, httpMethod: String, completion: @escaping (_ result: [String:AnyObject]) -> Void)
    {
        let request = NSMutableURLRequest(url: url)
        request.httpMethod = httpMethod
        
        let session = URLSession.shared

        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            
            if((error) != nil){
                var errorResponse = [String : AnyObject]()
                errorResponse["Error"] = STATUS_CODE_REQUEST_TIMEOUT as AnyObject
                errorResponse["StatusCode"] = STATUS_CODE_REQUEST_TIMEOUT as AnyObject
                completion(errorResponse)
 
            }else{
                
                let httpResponseURL = response as? HTTPURLResponse
                var errorResponse = [String : AnyObject]()
                if(httpResponseURL?.statusCode == STATUS_CODE_SUCCESS){
                    if data == nil{
                            errorResponse["Error"] = STATUS_CODE_UNABLE_TO_CONNECT as AnyObject
                            errorResponse["StatusCode"] = STATUS_CODE_UNABLE_TO_CONNECT as AnyObject
                            completion(errorResponse)
                    }else{
                            do{
                                let statusCode = httpResponseURL?.statusCode
                                let responseData  = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
                                let responseDic = ["Data" : responseData, "StatusCode" : statusCode!, "Error" : 0]
                                completion(responseDic as [String : AnyObject])
                       
                            }
                            catch _ as NSError {
                                errorResponse["Error"] = STATUS_CODE_UNKNOWN_ERROR as AnyObject
                                errorResponse["StatusCode"] = STATUS_CODE_UNKNOWN_ERROR as AnyObject
                                completion(errorResponse)
                            }
                    }
            }else{

                let error = String(decoding: data ?? Data(), as: UTF8.self)
                debugPrint(error)
                errorResponse["Error"] = httpResponseURL?.statusCode as AnyObject
                errorResponse["StatusCode"] = httpResponseURL?.statusCode as AnyObject
                completion(errorResponse)
            }
        }
        })
        task.resume()
    }
}
